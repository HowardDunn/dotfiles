syntax on
set number
set autoindent
set ignorecase
set smartcase

" no .swp files
set noswapfile

" tabs are 4 spaces wide, transform tabs into sapces
set shiftwidth=4
set softtabstop=4
set tabstop=4
set expandtab

" keep x lines around cursor
set scrolloff=15

" increase limit for max number of files to open in tabs with `vim -p`
set tabpagemax=256

" shorter timeout after ESC O
set ttimeoutlen=100

" move up and down inside of wrapped lines
noremap j gj
noremap k gk

" use <space> to switch tabs
nnoremap <space>i gt
nnoremap <space>u gT

" use <space>hjkl to switch between splits
nnoremap <space>h <C-w>h
nnoremap <space>j <C-w>j
nnoremap <space>k <C-w>k
nnoremap <space>l <C-w>l

" use <space>HJKL to move splits
nnoremap <space>H <C-w>H
nnoremap <space>J <C-w>J
nnoremap <space>K <C-w>K
nnoremap <space>L <C-w>L

" Let's save undo info!
if !isdirectory($HOME."/.vim")
    call mkdir($HOME."/.vim", "", 0770)
endif
if !isdirectory($HOME."/.vim/undo-dir")
    call mkdir($HOME."/.vim/undo-dir", "", 0700)
endif
set undodir=~/.vim/undo-dir
set undofile

" highlight long lines
augroup vimrc_autocmds
    autocmd BufEnter * highlight OverLength ctermbg=darkred ctermfg=white
    autocmd BufEnter * match OverLength /\%81v./
augroup END

" highlight OverLength ctermbg=darkred ctermfg=white guibg=#FFD9D9
" match OverLength /\%81v.*/

" highlight OverLength ctermbg=red ctermfg=white guibg=#592929
" let &colorcolumn=join(range(81,81),",")



" mks! ~/.vim/sessions/ ... specfified or current session
" wqa - optionally
" current session is set by ~/bin/vs (or alternativelly by this function)

" KNOWN BUG - session cannot be set from within vim because '!...' spawns a
" new shell -> TODO track active sessions using /tmp directory and tty id
function! s:MksFunc(quit, ...)
    if a:0 == 0
        if !empty($CURRENT_VIM_SESSION)
            mks! ~/.vim/sessions/$CURRENT_VIM_SESSION
        else
            echom "ERR: No active session!"
        endif
    elseif a:0 == 1
        execute 'mks! ~/.vim/sessions/' . a:1 
        execute '!export CURRENT_VIM_SESSION=' . a:1 
    else
        echom "ERR: Too many args"
    endif
    
    if a:quit == "quit"
        wqa
        execute '!unset CURRENT_VIM_SESSION'
    endif 
endfunction

" save session to specified of current session
command! -nargs=* M call s:MksFunc("-",<f-args>) 

" save session to specified of current session and quit and save all files
command! -nargs=* Mq call s:MksFunc("quit",<f-args>) 


" vimdiff syntax hotfix


" predefine whitespace chars for :set list
set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<




" Vundle START
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'vim-airline/vim-airline'
Plugin 'airblade/vim-gitgutter'
" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
" Plugin 'tpope/vim-fugitive'
" Plugin 'scrooloose/syntastic'
Plugin 'Robitx/vim-yapf-autopep8'
" plugin from http://vim-scripts.org/vim/scripts.html
Plugin 'L9'
" Git plugin not hosted on GitHub
"Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
"Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
"Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
"Plugin 'ascenator/L9', {'name': 'newL9'}
"Plugin 'NLKNguyen/papercolor-theme'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

let g:syntastic_check_on_open = 1
let g:flake8_show_quickfix=1
let g:syntastic_python_checkers = ['flake8']
" let g:autopep8_disable_show_diff=1
" "
" let g:autopep8_ignore="E226,E24,W6,E309"
"

"set t_Co=256   " This is may or may not needed.
"set background=light
" colorscheme PaperColor
